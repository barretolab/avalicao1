<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Lista de Clientes</title>
		<link rel="stylesheet" href="css/lojinha.css">
	</head>
	<body>
		<h1>Lista de Clientes</h1>
	
		<table border="1">
			<tr>
				<th>Nome</th>
				<th>Email</th>
			</tr>
			<c:forEach items="${requestScope.lista}" var="c">
				<tr>
					<td>${c.nome}</td>
					<td>${c.email}</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>