<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%int i = 0;%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Carrinho de produtos</title>
		<link rel="stylesheet" href="css/lojinha.css">
	</head>
	<h1>Itens do carrinho</h1>

	<body>
		<table border="1">
			<tr>
				<th>C�digo</th>
				<th>Produto</th>
				<th>Quantidade</th>
				<th>********</th>
			</tr>
	
			<c:forEach items="${sessionScope.carrinho}" var="c">
				<tr>
					<%i++;%>
					<td>${c.id}</td>
					<td>${c.descricao}</td>
					<td><%=i%></td>
					<td><small><a href="carrinho.sec?cmd=remove&produto=<%=i%>" class="action">Excluir</a></small>
					</td>
				</tr>
			</c:forEach>
		</table>
	
		<br>
		<a href="comprar.sec">Voltar</a>
	</body>
</html>
