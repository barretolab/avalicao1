package edu.ifsp.lojinha.persistencia;

import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;

public class ClienteDAO {
	public Cliente save(Cliente cliente) {
		System.out.println("ClienteDAO.save()");
		return cliente;
	}
	
	public List<Cliente> listAll() {
		List<Cliente> lista = List.of(
				new Cliente("Adriana Alves", "adrianaAlves@gmail.com"),
				new Cliente("Bernardo Bento", "bernardoBento@hotmail.com"),
				new Cliente("Carlos Cardoso", "carlosCar@hotmail.com"),
				new Cliente("D�cio Dias", "decioDias@gmail.com")
				);
		return lista;
	}
}
